﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Diagnostics;

using System.IO;

namespace Listener
{
    class Program
    {
        static int port = 31415;    //3.1415 egy PI-llanat alatt eszébe jut az embernek
        static int max_byte = 10000000;
        static byte[] adatok = new byte[max_byte];
        static int size;

        static string filename = "magnes.adat";

        static void Main(string[] args)
        {
            while(konzol());
        }

        static bool konzol()
        {
            Console.WriteLine("Az adatok figyeléséhez írjon A-t, kilépéshez Q");
            string str = Console.ReadLine();

            if (str == "Q" || str == "q")
                return false;
            else
                if (str == "A" || str == "a")
                {
                    Listen();

                    if (adatok == null)
                    {
                        Console.WriteLine("Hiba történt, próbálja újra!");
                        return true;
                    }

                    Save();

                }
                else
                    Console.WriteLine("Ezt nem értem.");

            
            return true;
        }

        static void Listen()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://*:" + port + "/");
            try
            {
                listener.Start();
                Console.WriteLine("Várakozás az adatokra...");

                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;

                HttpListenerResponse response = context.Response;

                //Tömb nullázása majd beolvasás
                adatok = new byte[max_byte];
                context.Request.InputStream.Read(adatok, 0, max_byte);

                int i = 0;
                while (i < max_byte && adatok[i] != 0)
                {
                    //Console.Write((char)adatok[i]);
                    //if ((char)adatok[i] == '\n') Console.WriteLine();

                    i++;
                }
                Console.WriteLine();
                Console.WriteLine("" + i + " byte-nyi adat érkezett.");
                size = i;


                string responseString = "Siker";
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                
                output.Close();
                listener.Stop();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        static void Save()
        {
            try
            {
                System.IO.FileStream stream = new FileStream(filename, FileMode.Create);
                stream.Write(adatok, 0, size);
                Console.WriteLine("Mentve " + filename + " néven.");
                stream.Flush();
                stream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        /*
        static void valogat()
        {
            try
            {
                StreamReader reader = new StreamReader(filename);

                //Kezdés időpontja
                int t_0 = int.Parse(reader.ReadLine().Substring(1));
                

                string[] str = reader.ReadLine().Split('\t');
                
                int t;
                long x = long.Parse(str[1]), y = long.Parse(str[2]), z = long.Parse(str[3]);
                
                long v, v_0 = (long) Math.Sqrt(x*x+y*y+z*z);

                while(str.Length == 4)
                {
                    t = int.Parse(str[0]) - t_0;
                    x = long.Parse(str[1]);
                    y = long.Parse(str[2]);
                    z = long.Parse(str[3]);
                    str = reader.ReadLine().Split('\t');

                    v = (long) Math.Sqrt(x*x+y*y+z*z);

                    if(Math.Abs(v - v_0) / v_0 > 1)


                }

                reader.Close();
            }
            catch (Exception e) { Console.WriteLine(e.ToString();}
        }*/
    }
}
