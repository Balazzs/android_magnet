package com.example.test_magnes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;

import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements SensorEventListener {

	private SensorManager mSensorManager;
    private Sensor sens;
    //Mennyi adatunk van
    private int numb, max_hossz = 500;
    //A kezd�si id�
    private long starttime;
    //Ezt fogja �tk�ldeni
	private String szoveg = "#";
	//A filen�v
    private String Filename = "magnes.adat";
    //Erre lehet �rogatni
    private TextView myText = null;
    //A start/stop gomb
    private Button gomb2;
    //Egy sz�vegmez� amibe sz�mok �rhat�k - meddig m�rjen
    private EditText editbox;
    //A g�p IP-je �s portja
    private EditText ip, port;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		LinearLayout lView = new LinearLayout(this);
		LinearLayout ll = new LinearLayout(this);
		ll.setOrientation(LinearLayout.VERTICAL);;
		
		//K�ld�s gomb
		Button gomb = new Button(this);
		
		gomb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            	write(Filename, szoveg);
            	safePostThread();
            }
        });
		
		gomb.setText("K�ld");
		lView.addView(gomb);
		
		//Start/Stop gomb
		gomb2 = new Button(this);
		
		gomb2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)
			{
				// Perform action on click
				if(((Button) v).getText() == "Start")
					Start();
				else
					Stop();
			}
		});
		
		gomb2.setText("Start");
		lView.addView(gomb2);
		
		//TextField
		editbox = new EditText(this);
		editbox.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
		editbox.setText("500");
		lView.addView(editbox);
		
		//Sz�veg
		myText = new TextView(this);
		myText.setText("My Text");

		ll.addView(myText);
		ll.addView(lView);
		
		setContentView(ll);

		Filename = Environment.getExternalStorageDirectory() + "/" + Filename;
		
        numb = 0;
        starttime = System.currentTimeMillis();
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        sens = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
        
        if(sens == null)
        	myText.setText("Nem tal�lhat� m�gneses szenzor :(");
        
        //Ha forgat�sb�l t�r vissza
      	if(savedInstanceState != null)
      	{
      		szoveg = savedInstanceState.getString("adatok");
      		starttime = savedInstanceState.getLong("kezdo_ido");
      		numb = savedInstanceState.getInt("numb");
      		editbox.setText(savedInstanceState.getString("editbox"));
      		return;
      	}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

	public void onSensorChanged(SensorEvent event) {
    	     	
		szoveg += event.timestamp + "\t" + event.values[0] + "\t" + event.values[1] + "\t" + event.values[2] + "\n";
    	
    	numb++;
    	if(numb == max_hossz)
    		Stop();
    	
    	//�rjon is ki valamit
    	myText.setText("X:" + event.values[0] + "\nY:" + event.values[1] + "\nZ:" + event.values[2] + "\ndb:" + numb + "/" + max_hossz + "\ndb/sec:" + (1.0 * numb / (System.currentTimeMillis() - starttime) *1000));
	}
     
     
	private void Stop()
    {
		gomb2.setText("Start");
    	
    	mSensorManager.unregisterListener(this, sens);
    	write(Filename, szoveg);
    	safePostThread();
    }
     
    private void Start()
    {
    	gomb2.setText("Stop");
    	
    	numb = 0;
    	
    	//Pr�b�ljuk meg �rtelmezni, hogy meddig tartson
    	try{max_hossz = Integer.parseInt(editbox.getText().toString());}catch(Exception e){};
    	
    	starttime = System.currentTimeMillis();
    	szoveg = "#" + starttime + "\n";
    	
    	mSensorManager.registerListener(this, sens, 0);
    }
     
     
    private void safePostThread()
    {
    	Thread thread = new Thread(new Runnable(){
		    @Override
		    public void run() {
		        try {
		            //Your code goes here
		        	postData();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    }
		});
		
		thread.start();
    }

    private void postData()
    {
    	String url = "http://192.168.0.12:31415/";
    	
    	try
    	{
    		HttpClient http = AndroidHttpClient.newInstance("MyApp");
	    	HttpPost method = new HttpPost(url);
	    	
	    	method.setEntity(new FileEntity(new File(Filename), "application/octet-stream"));
	    	
	    	HttpResponse response = http.execute(method);
	    	
	    	//A v�lasz kiolvas�sa
	    	byte[] buffer = new byte[5];
	    	response.getEntity().getContent().read(buffer);
	    	
	    	//N�zz�k siker�lt-e
	    	boolean sikerult = true;
	    	char[] s = "Siker".toCharArray();
	    	for(int i = 0; i < s.length && i < buffer.length; i++)
	    		if(buffer[i] != (byte) s[i])
	    			sikerult = false;
	    	
	    	if(sikerult)
	    		//A l�tez� legfur�bb �s legegyszer�bb megold�s, hogy m�sik thread-r�l megjelen�thess�nk Toastokat
	    		this.runOnUiThread(new Runnable()
	            {
	                public void run(){ Toast.makeText(getBaseContext(), "Adatok �tk�ldve", Toast.LENGTH_LONG).show();}
	            });
	    		
	    	
    	} catch (Exception e)
    	{
    		System.out.println(e.toString());
    		this.runOnUiThread(new Runnable()
            {
                public void run(){ Toast.makeText(getBaseContext(), "Hiba a f�jl�tk�ld�s sor�n", Toast.LENGTH_LONG).show();}
            });
    	}
    }
     
    public Boolean write(String fname, String fcontent){
    	try
    	{
    		File file = new File(fname);
	
	    	if (!file.exists())
	    	 file.createNewFile();    	 
	    	 
	    	FileWriter fw = new FileWriter(file.getAbsoluteFile());
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	bw.write(fcontent);
	    	bw.close();
	    	Log.d("Suceess","Sucess");
	    	return true;
    	}
    	catch (IOException e)
    	{
	    	e.printStackTrace();
	    	return false;
    	}
    }
    
    //Hogy forgat�sn�l ne vesszen el minden inform�ci� �s az eg�sz vil�gegyetem
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString("adatok", szoveg);
        savedInstanceState.putLong("kezdo_ido", starttime);
        savedInstanceState.putInt("numb", numb);
        savedInstanceState.putString("editbox", editbox.getText().toString());
        
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }
}
